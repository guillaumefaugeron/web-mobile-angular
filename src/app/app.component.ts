import {Component, OnInit} from '@angular/core';
import {v4 as uuid} from 'uuid';
import {SwUpdate} from "@angular/service-worker";
import {Platform} from "@angular/cdk/platform";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'todo';

  filter: 'all' | 'active' | 'done' = 'all';
  allItems = [
    {description: 'eat', done: true, id: uuid()},
    {description: 'sleep', done: false, id: uuid()},
    {description: 'play', done: false, id: uuid()},
    {description: 'laugh', done: false, id: uuid()},
  ];
  contacts: any;

  get items() {
    if (this.filter === 'all') {
      return this.allItems;
    }
    return this.allItems.filter((item) => this.filter === 'done' ? item.done : !item.done);
  }

  addItem(description: string) {
    this.allItems.unshift({
      id: uuid(),
      description,
      done: false
    });
  }

  removeItem(id: string) {
    this.allItems.splice(this.allItems.findIndex((object) => {
      return object.id === id;
    }), 1);

  }

  public async loadContactPwa(): Promise<void> {
    if ("contacts" in navigator) {
      const props = ["name"];
      const opts = {multiple: true};

      // @ts-ignore
      this.contacts = await navigator.contacts.select(props, opts);
      console.log(Object.keys(this.contacts))
    } else {
      console.log("Your browser doesn't support Contact Picker API");
    }

  }

}
